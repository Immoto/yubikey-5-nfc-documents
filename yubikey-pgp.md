# YubiKey 5 NFC にて PGP key を取り込む

YubiKey 5 NFCを購入した。PGPの鍵を入れておけるとあったので、実際に試した。
OSは、macOS Mojave、ハードは、MacBook Proである。

最初に、
https://support.yubico.com/support/solutions/articles/15000006420-using-your-yubikey-with-openpgp
を読んだのだが、必要もないのに”Insert the YubiKey into the USB port.”との記述が散見される。
そのため、ちょっと信頼できないと感じた。

そこで、いろいろと検索した結果、以下の開発者向けのドキュメントに従うことでうまくいった。

https://developers.yubico.com/PGP/Importing_keys.html

注意点としては、

- キー長を 2048としておくこと
- カード側のpgpのPIN,Admin PINの初期値を忘れた場合の初期化の為に、ykmanコマンドをインストールしておくこと。

なお、PIN,Admin PINとあるが、PIV側のPIN,Admin PINと混同しないこと。
pgpカード(YubiKey側)のPINとAdmin PINのことである。
